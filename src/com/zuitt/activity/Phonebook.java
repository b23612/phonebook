package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {
    public Phonebook(){}
    public Phonebook(Contact contact){
        this.contacts.add(contact);
    }
    public ArrayList<Contact> contacts = new ArrayList<Contact>();

    public ArrayList<Contact> getContacts() {
        return contacts;
    }
    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }

}