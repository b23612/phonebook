package com.zuitt.activity;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args){

        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact();
        Contact contact2 = new Contact();
        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        contact1.setName("John Doe");
        contact1.setContactNumber("+639152468596");
        contact1.setAddress("my home in Quezon City");

        contact2.setName("Jane Doe");
        contact2.setContactNumber("+639162148573");
        contact2.setAddress("my home in Caloocan City");

        System.out.println("---------------------");
        System.out.println(contact1.getName());
        System.out.println("---------------------");
        System.out.println(contact1.getName() + " has the following registered number:");
        System.out.println(contact1.getContactNumber());
        System.out.println(contact1.getName() + " has the following registered address:");
        System.out.println(contact1.getAddress());

        System.out.println("---------------------");
        System.out.println(contact2.getName());
        System.out.println("---------------------");
        System.out.println(contact2.getName() + " has the following registered number:");
        System.out.println(contact2.getContactNumber());
        System.out.println(contact2.getName() + " has the following registered address:");
        System.out.println(contact2.getAddress());

       if (phonebook.getContacts().isEmpty()){
            System.out.println("Phonebook is empty. Please add a contact.");
       }

    }
}
